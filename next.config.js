/** @type {import('next').NextConfig} */
const nextConfig = {
  transpilePackages: ["@stripe/firestore-stripe-payments"],
  reactStrictMode: true,
  images: {
    domains: ["image.tmdb.org", "rb.gy", "s3-us-west-2.amazonaws.com"],
  },
};

module.exports = nextConfig;
