// Import the functions you need from the SDKs you need
import { initializeApp, getApp, getApps } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAzEk1cvzrOTkpDm4EMRYEZB0yRaYC2vMc",
  authDomain: "netflix-clone-ce005.firebaseapp.com",
  projectId: "netflix-clone-ce005",
  storageBucket: "netflix-clone-ce005.appspot.com",
  messagingSenderId: "739084500671",
  appId: "1:739084500671:web:8587cdc39dc8f92ded500a",
  measurementId: "G-LLCHTT2HF6",
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();
const auth = getAuth();

export default app;
export { auth, db };
